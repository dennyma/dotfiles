""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Text, tab and indent related
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set smarttab
set shiftwidth=4                " One tab == four spaces.
set tabstop=4                   " One tab == four spaces.

set number
set clipboard=unnamedplus		" Copy/Paste between vim and other programs.

set laststatus=2
