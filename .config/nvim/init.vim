set nocompatible              " be iMproved, required

packadd! dracula_pro
syntax enable
let g:dracula_colorterm = 0
let g:dracula_italic = 0
colorscheme dracula_pro_van_helsing
set ignorecase 

set clipboard=unnamedplus       " Copy/paste between vim and other programs.
set noswapfile

set number
set smarttab
set shiftwidth=2
set tabstop=2

set laststatus=2
set showtabline=2
set noshowmode
set t_Co=256

set mouse=nicr
set mouse=a
