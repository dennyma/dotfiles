# Dotfiles

This is personal repo to store Dotfiles.

### How To Use

```sh
git clone --bare https://gitlab.com/dennyma/dotfiles.git $HOME/.cfg
function config {
   /usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME $@
}

config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
  else
    echo "Backing up pre-existing dot files.";
    config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -d $'\n' sh -c 'for arg do mkdir -p .config-backup/"$arg"; mv "$arg" .config-backup/"$arg"; done;' _
fi;
config checkout
config config status.showUntrackedFiles no
```
